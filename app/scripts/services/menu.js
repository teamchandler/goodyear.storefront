'use strict';

angular.module('chandler.schneider')
  .service('MenuService', function Menu(UtilService, CryptoService) {
        this.get_default_menu = function(){
            return UtilService.get_default_menu();
        }

           var check_user = function(){
            var u = UtilService.get_from_localstorage('user_info');
            //console.log(u);
            if ((u == "") || (u == null)) {
                // send to login
               // $state.go('login.login');
                return false;
            }
        }




            this.get_menu = function(){
            var l_menu = UtilService.get_menu().slice();
            var annectos_program_admin = UtilService.get_anenctos_program_admin();
            var schneider_program_admin = UtilService.get_schneider_program_admin();
            var u = UtilService.get_from_localstorage("user_info");
            var show_admin = false;
            var logged_user = true;

            logged_user = check_user();
            if(logged_user == false)
            {
                  l_menu[6].link = "#/rewards";
            }
             else
            {
                   l_menu[6].link = "/rewards";

            }


            if(u!=null){
            for (var i=0; i< annectos_program_admin.length; i++){
                if(u.email_id!=null && u.email_id!=""){
                    if (u.email_id.toLowerCase() == annectos_program_admin[i]) {
                        show_admin=true;
                        user_type="annectos admin";
                    }
                }

            }
            for ( i=0; i< schneider_program_admin.length; i++){
                if(u.email_id!=null && u.email_id!="") {
                    if (u.email_id.toLowerCase() == schneider_program_admin[i]) {
                        show_admin = true;
                        user_type="schneider admin";
                        break;
                    }
                }
            }
                // encrypt user_info
                // var u = UtilService.get_from_localstorage("user_info");
//               var key = CryptoService.encrypt(JSON.stringify(u));
//                   key=encodeURIComponent(key);
//                for (i=0; i < l_menu.length; i++){
//                    if (l_menu[i].state == ""){
//                        l_menu[i].link =l_menu[i].link + "/index.html#/sso/" + key ;
//                    }
//                }

            }

            if (!show_admin){
                if (!show_admin){
                    for ( i=0; i< l_menu.length; i++){
                        if (l_menu[i].admin  == 1) {
                            l_menu.splice(i,1);
                        }
                    }
                }
            }


            return l_menu;
        }
  });
