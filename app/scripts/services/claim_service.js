/**
 * Created by developer6 on 6/12/2014.
 */
'use strict';

angular.module('chandler.schneider')
    .service('ClaimService', function Menu(UtilService) {

        this.get_retailer_list = function($http, $q){

            var apiPath = UtilService.get_api() +
                '/retailer/list';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }


        this.get_user_company_name = function($http, $q, company,srch_key){

            var apiPath = UtilService.get_api() +
                '/get/user/company/name/by/'+ company + "/" + srch_key ; ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }


        this.save_invoice = function($http, $q, dataobj){

            var apiPath =  UtilService.get_api() +
                '/invoice/schneider/add' ;

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

        this.add_override_sku = function($http, $q, dataobj){

            var apiPath =  UtilService.get_api() +
                '/override/sku/add' ;

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }


        this.get_all_company_name = function($http, $q){
            var apiPath = UtilService.get_api() +
                '/user/company';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

        this.get_user_by_comp_name = function($http, $q,comp_name ){
            var apiPath = UtilService.get_api() +
                '/user/details/'+ comp_name ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

        this.get_invoice_by_user_name = function($http, $q,user_name ){
            var apiPath = UtilService.get_api() +
                '/get/invoice/detail/by/user/'+ user_name ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

        this.get_invoice_by_retailer_name = function($http, $q,retailer_name ){
            var apiPath = UtilService.get_api() +
                '/get/invoice/detail/by/retailer/'+ retailer_name ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }


        this.get_invoce_by_status_comp_name = function($http, $q,status,company_name ){
            var apiPath = UtilService.get_api() +
                '/search/invoice/by/'+ status + "/" + company_name ;


            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }



        this.search_by_sku = function($http, $q,sku ){
            var apiPath = UtilService.get_api() +
                '/search/by/sku/'+ sku ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }



        this.search_by_part_code = function($http, $q, sku ){

            var dataobj={};
            dataobj.part_code=sku;

            var apiPath =  UtilService.get_api() +
                '/search/by/part/code' ;

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }


        this.search_sku_list = function($http, $q,sku ){
            var apiPath = UtilService.get_api() +
                '/search/sku/list/by/sku/'+ sku ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

        /************Added By Dipanjan**************/

        this.invoice_details_by_number = function($http, $q, claim_id ){
            var apiPath = UtilService.get_api() +
                '/invoice/detail/by/id/'+ claim_id ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

        this.save_sku = function($http, $q, dataobj){

            var apiPath =  UtilService.get_api() +
                '/sku/add' ;

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }



        this.update_claim_details = function($http, $q, claim_id,inv_amount,claim_details,modified_by,modified_date,status,approval_comments,amount_track,inv_date_track,invoice_date){

            var dataobj={};
            dataobj.claim_id=claim_id;
            dataobj.claim_details=claim_details;
            dataobj.status=status;
            dataobj.approval_comments=approval_comments;
            dataobj.invoice_amount = inv_amount;
            dataobj.amount_track = amount_track;
            dataobj.inv_date_track = inv_date_track;
            dataobj.invoice_date = invoice_date;
            dataobj.modified_by = modified_by;
            dataobj.modified_date = modified_date;

            var apiPath =  UtilService.get_api() +
                '/invoice/claim/details/update' ;

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }

		this.trigger_email_srvc = function($http, $q, dataobj){

            var apiPath =  'http://emailblustengin.annectos.net:3000/email' ;

            console.log(dataobj);
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }



        this.get_top_si = function($http, $q, claim_id ){
            var apiPath = UtilService.get_api() +'/top/si' ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }



        this.get_top_si_by_region = function($http, $q, claim_id ){
            var apiPath = UtilService.get_api() +'/top/si/by/region' ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }


        this.get_si_by_region = function($http, $q, claim_id ){
            var apiPath = UtilService.get_api() +'/si/by/region' ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }


        this.get_normal_si_qualifier = function($http, $q, claim_id ){
            var apiPath = UtilService.get_api() +'/normal/program/qualifier/si' ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }


        this.get_special_si_qualifier = function($http, $q, claim_id ){
            var apiPath = UtilService.get_api() +'/special/program/qualifier/si' ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }
    });
