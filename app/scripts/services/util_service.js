'use strict';

angular.module('chandler.schneider')
  .service('UtilService', function UtilService(localStorageService,
                                               NotificationService) {

    // This service should be the ONLY interface to all config and localstorages
        this.get_default_title = function (){
            return default_title;
        }
        this.get_default_menu = function (){
            return default_menu;
        }
        this.get_menu = function (){
            return menu;
        }
        this.get_api = function () {
            return api;
        };
        this.get_company = function () {
            return company;
        };
        this.get_user_type = function () {
            return user_type;
        };

        this.get_encryption_phrase = function () {
            return encryption_phrase;
        };

        this.get_anenctos_program_admin = function () {
            return annectos_program_admin;
        };
        this.get_schneider_program_admin = function () {
            return schneider_program_admin;
        };

        this.get_state_list = function () {
            return state_list;
        };
        this.get_main_page_slider = function () {
            return adItems.landing_page_main_scroller;
        };

        this.get_program_start_date = function () {
            return prog_start_dt;
        };

        this.get_program_end_date = function () {
            return prog_end_dt;
        };

        this.get_to_email = function () {
            return to_email;
        };


        this.get_from_localstorage = function (key){
            return localStorageService.get(key);
        }
        this.set_to_localstorage = function (key, val){
            return localStorageService.add(key, val);
        }

        this.change_header = function(header_text){
            NotificationService.change_header("change header", (header_text));
        }
        this.select_menu = function(state_name){
            NotificationService.send_notification("state change", state_name);
        }
        this.send_notification = function(msg, data){
            NotificationService.send_notification(msg, data);
        }
//        this.load_config = function($http, $q){
//
//        }

  });
