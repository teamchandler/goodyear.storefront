'use strict';


angular.module('chandler.schneider')
  .service('MasterService', function MasterService(UtilService) {

        this.user_login = function($http,$q,credential){

            var apiPath =  UtilService.get_api() +  '/login/by';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:credential,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }

        this.change_password = function($http,$q,credential){

            var apiPath =  UtilService.get_api() +  '/user/change/password';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data:credential,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("Data Error");


            })
            return deferred.promise;


        }

        this.get_profile_info = function($http, $q, email_id ){
            var apiPath = UtilService.get_api() +
                '/userinfo/by/user/'+ email_id ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }



        this.save_user_basic_info = function($http, $q, dataobj){

            var apiPath =  UtilService.get_api() +
                '/user/basic/info/update' ;

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }


        this.update_user_address = function($http, $q, dataobj){

            var apiPath =  UtilService.get_api() +
                '/user/basic/address/update' ;

            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                ContentType: 'application/json'

            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }
        this.upload = function ($http, $q, f, content_type,url){
            var deferred = $q.defer();
            $http({
                method: 'PUT',
                url: url,
                data : f
//                type: content_type
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }
        this.get_update_url = function($http, $q , filename){
            var apiPath = UtilService.get_api() +
                '/aws/get/upload_url/' + filename ;

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("Data Error");
            })
            return deferred.promise;
        }



        this.get_redemption_details = function ($http, $q, user_id) {
            var apiPath = UtilService.get_api() + '/order/detail/by/email/' + user_id ;
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        }



        this.get_redemptionmore_details = function ($http, $q,order_id) {
            var apiPath = UtilService.get_api() + '/get/shipping_details/by/order_id/' + order_id ;
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        }
        this.get_order_information = function ($http, $q,order_id) {
            var apiPath = UtilService.get_api() + '/get/order_information/by/order_id/' + order_id ;
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
            return deferred.promise;

        }


    });

