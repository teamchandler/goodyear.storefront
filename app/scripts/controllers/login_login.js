'use strict';

angular.module('chandler.schneider')
  .controller('LoginLoginCtrl', function (  $scope, $http,$q, $state,
                                            UtilService, MasterService
                                            ) {

        $scope.login = function(){
            var credential={};
            credential.company = UtilService.get_company();
            credential.email_id = $scope.email_id;
            credential.password = $scope.password;
            MasterService.user_login($http, $q, credential)
                .then
                    (
                        function (data) {
                            console.log(data[0][0].msg);
                            if (data[0][0].msg ==='login successful'){
                                alert("Login Successful. Now you will be redirected to home page ...")
                                UtilService.set_to_localstorage("user_info", data[0][0]);
                                UtilService.send_notification("Login Complete", "");
                                $state.go("main.home");
                            }
                            else{
                                alert("Sorry - your credentials don't match with what we have in file")
                                UtilService.send_notification("Login Complete", "");
                                UtilService.set_to_localstorage("user_info", "");
                            }
                        },

                        function () {
                            //Display an error message
                            $scope.error = error;
                        }
                    );

        }

        init();
        function init()
        {
            UtilService.change_header($state.current.data.header_label);
            UtilService.select_menu($state.current.name);
            UtilService.set_to_localstorage("user_info", "");
            $scope.username = "";
            $scope.password = "";
            UtilService.send_notification("LogOut","");

        };




  });
