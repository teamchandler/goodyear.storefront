/**
 * Created by developer4 on 6/12/2014.
 */


angular.module('chandler.schneider')
    .controller('programCtrl', function ($scope , $state,  $http, $q,
                                         ClaimService, UtilService) {


      //  $rootScope.invoice_data;

        $scope.comp_name_list = [];

        $scope.show_loader=false;

        var login_user_type=UtilService.get_user_type();

        if(login_user_type=="schneider admin"){
            $scope.status = 'Schneider Verification Required';
        }else{
            $scope.status = 'pending';
        }


        init();

        $scope.$watch('status', function(){

            $scope.show_loader=true;
            get_invoce_by_status_comp_name();

        });
//        $scope.$watch('company_name', function(){
//
////            get_invoce_by_status_comp_name();
//
//        });

        function init() {


            UtilService.change_header($state.current.data.header_label);
            UtilService.select_menu($state.current.name);
            $scope.show_cond = 'h';
//            get_user_company_name();
           // get_all_company_name();
             get_invoce_by_status_comp_name();

           // get_invoce_by_status_comp_name();
//            get_retailer_list()
            //  get_region_level_wise();
            //  get_all_program();
        }

        $scope.get_autocomplete_values = function () {
            var search_comp_name = $scope.company_name;
            if (search_comp_name.length > 2) {
                get_user_company_name();
            }
        }


        function get_user_company_name(){

            var company=UtilService.get_company();
            var srch_key=$scope.company_name;

            ClaimService.get_user_company_name($http, $q, company, srch_key)
                .then(function (data) {
                    $scope.comp_name_list =[];
                    var comp_name_array=[];
                    angular.forEach(data, function(item) {
                        comp_name_array.push(item.company_name)
                    });
                    $scope.comp_name_list =comp_name_array;
//                    $scope.retailer_list = data;


                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });



        }


        function get_retailer_list(){

            ClaimService.get_retailer_list($http, $q)
                .then(function (data) {

                    $scope.retailer_list = data;

                    if (data[0].message != "no result found") {
                        $scope.not_end_of_list = true;
                    }
                    else {
                        $scope.s_data = data;
                        $scope.end_of_list = false;
                    }
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

        function get_all_company_name() {
            ClaimService.get_all_company_name($http, $q)
                .then(function (data) {
                    $scope.company_list = data;
                    if (data[0].message != "no result found") {
                        $scope.not_end_of_list = true;
                    }
                    else {
                        $scope.region_list = data;
                        $scope.end_of_list = false;
                    }

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }



       // $scope.user_name="";


        $scope.get_invoice_by_retailer_name = function () {
            ClaimService.get_invoice_by_retailer_name($http, $q, $scope.retailer_name)
                .then(function (data) {

                    $scope.invoice = data;

                    if (data[0].message != "no result found") {
                        $scope.not_end_of_list = true;
                    }
                    else {
                        $scope.region_list = data;
                        $scope.end_of_list = false;
                    }

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }


        $scope.download_invoice = function (sup_doc) {
            window.open(sup_doc);
        }

        $scope.get_invoce_by_comp_name = function (sup_doc) {
            get_invoce_by_status_comp_name();
        }

        function get_invoce_by_status_comp_name () {

            var status=$scope.status;
            var company_name=$scope.company_name;
            if ($scope.company_name == "" || typeof $scope.company_name === 'undefined' ){
                company_name='undefined';
            }

            ClaimService.get_invoce_by_status_comp_name($http, $q, status, company_name)
                .then(function (data) {


                    $scope.invoice = data;
                    $scope.show_loader=false;

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

//        $scope.get_user_by_comp_name = function () {
//            ClaimService.get_user_by_comp_name($http, $q, $scope.sel_comp_name)
//                .then(function (data) {
//
//                    $scope.user_list = data;
//
//                    if (data[0].message != "no result found") {
//                        $scope.not_end_of_list = true;
//                    }
//                    else {
//                        $scope.region_list = data;
//                        $scope.end_of_list = false;
//                    }
//
//                },
//                function () {
//                    //Display an error message
//                    $scope.error = error;
//
//                });
//
//        }

        $scope.invoice_details_by_number = function(claim_id)
        {
            $state.go(  "main.program_details", {'claim_id' : claim_id});
        }


        $scope.save_sku = function()
        {
            ClaimService.save_sku($http, $q, $scope.sku_data)
                .then(function (data) {
                    $scope.show_cond='s';
                    alert(data);

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });


        }

    });