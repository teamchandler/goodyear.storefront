
'use strict';

angular.module('chandler.schneider')
  .controller('PageCtrl', function ($scope,$state,
                                    UtilService,  NotificationService, MenuService) {

        $scope.$on('change header', function() {
            $scope.title = NotificationService.header_label;

        });
        $scope.$on('state change', function() {
            $scope.default_menu = NotificationService.notification_data;
        });
        $scope.$on('Login Complete', function() {
            $scope.menu_data = MenuService.get_menu();
            calculate_points();
        });

        $scope.$on('LogOut', function() {
            calculate_points();
        });

        var init = function(){
            $scope.menu_data = MenuService.get_menu();
           // check_user();
           // $scope.default_menu = MenuService.get_default_menu();
        }
        init();
        calculate_points();

        function calculate_points(){
            var points_user = UtilService.get_from_localstorage('user_info');
            if(points_user!=null && points_user!="") {
                $scope.points= parseInt(points_user.total_point) ;
            }else{
                $scope.points=0;
            }
        }




  });
