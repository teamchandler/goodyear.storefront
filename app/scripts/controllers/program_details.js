/**
 * Created by developer6 on 6/15/2014.
 */
angular.module('chandler.schneider')
    .controller('programdetlCtrl', function ($scope,$state,$stateParams,UtilService,
                                             $http, $q, ClaimService ,$location ) {


        var claim_id=$stateParams.claim_id;
        var login_user_type=UtilService.get_user_type();
        var inv_url=$location.absUrl();


        /*Start*************Test Calender Panel*****************/
        $scope.today = function() {
            $scope.invoice_date = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.invoice_date = null;
        };

        // Disable weekend selection
        $scope.disabled = function(date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        $scope.toggleMin = function() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.initDate = new Date('2016-15-20');
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        /*End*************Test Calender Panel*****************/


        $scope.user_type=login_user_type;
//        alert($scope.user_type);

        init();

        function init(){
       console.log($state.current);
       console.log ($stateParams.claim_id);
     //  alert('hi');
       get_retailer_list();
       invoice_details_by_number(claim_id);
   }

        function get_retailer_list(){

            ClaimService.get_retailer_list($http, $q)
                .then(function (data) {

                    $scope.distributor_list = data;

                    if (data[0].message != "no result found") {
                        $scope.not_end_of_list = true;
                    }
                    else {
                        $scope.s_data = data;
                        $scope.end_of_list = false;
                    }
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

        var prev_inv_date="";

        function invoice_details_by_number(claim_id)
        {
            ClaimService.invoice_details_by_number($http, $q, claim_id)
                .then(function (data) {
                    $scope.inv_detail = data[0];

                    var inv_dt=new Date(data[0].invoice_date);

                    var dt=inv_dt.getDate()*1;
                    if(dt<10){
                        dt="0"+dt;
                    }
                    var mon=(inv_dt.getMonth()*1+1);
                    if(mon<10){
                        mon="0"+mon;
                    }
                    var invoice_date=dt+"/"+mon+"/"+inv_dt.getFullYear();
                    prev_inv_date=invoice_date;
//                    alert(invoice_date);
                    $scope.invoice_date=invoice_date;

                    $scope.sku_details = data[0].claim_details;
                    sku_array=$scope.sku_details;
                    $scope.comment_list=data[0].approval_comments;
                    $scope.amount_list = data[0].amount_track;

                    $scope.inv_date_list = data[0].inv_date_track;

                    if(data[0].status=="Verified & Approved"){
                        $scope.isenable=true;
                    }else
                    {
                        $scope.isenable=false;
                    }

                    if(login_user_type=="schneider admin") {
                        if (data[0].status == "Schneider Verification Required") {
                            $scope.isenable = false;
                        }
                        else{
                            $scope.isenable = true;
                        }
                    }

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }
        var sku_array = [];
        $scope.add_sku = function()
        {


            if ($scope.sku_data == "" || typeof $scope.sku_data === 'undefined' ){
                alert ("Please enter Part CODE");
                return false;
            }


            var sku=$scope.sku_data;
            var found=false;
            angular.forEach(sku_array, function(item) {
                if(item.sku==sku)
                {
                    alert('already exists this Part CODE');
                    found=true;
                }
            });

            if(found==false) {

                ClaimService.search_by_part_code($http, $q, sku)
                    .then(function (data) {
                        var sku_status = "verified";
                        if (data.length == 0) {
                            sku_status = "not-verified";
                        }
                        $scope.sku_status_class = sku_status;
                        var sku_details = {sku: sku, status: sku_status};
                        sku_array.push(sku_details);
                        $scope.sku_details = sku_array;
                        $scope.sku_data = "";
                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }

        }

        $scope.search_by_sku=function(sku){

          alert(sku)  ;

        }

        $scope.delete_by_sku=function(osku){

            var sku_length=sku_array.length;
            for (var i=0; i< sku_length ; i++){
                if(sku_array[i].sku==osku.sku){
                    sku_array.splice(i,1);
                    break;
                }
            }

            $scope.sku_details = sku_array;

        }

        $scope.verify_by_sku=function(sku){

            alert(sku)  ;

        }

        $scope.part_verify_status=function(sku_staus){

            var part_class="";
            if(sku_staus=="verified"){
                part_class= "part_no_verify_status";
            }
            else{
                part_class="part_no_not_verify_status";
            }

           return part_class;

        }

        $scope.part_not_verify_status=function(sku_staus){

            var part_class="";
            if(sku_staus=="not-verified"){
                part_class= "part_no_verify_status";
            }
            else{
                part_class="part_no_not_verify_status";
            }

           return part_class;

        }

        $scope.approve_reject_by_sku=function(osku){

//            alert(osku)  ;

            var sku_status=osku.status;
            var new_status="";

            if(osku.status=="not-verified"){
                 new_status="verified";
            }else
            {
                 new_status="not-verified";
            }

            var upd_sku={sku:osku.sku,status:new_status};


            var sku_length=sku_array.length;
            for (var i=0; i< sku_length ; i++){
                if(sku_array[i].sku==osku.sku){
                    sku_array[i]=upd_sku;
                    break;
                }
            }

            $scope.sku_details = sku_array;

            if(sku_status=="not-verified"){

                var override_sku={
                    claim_id: claim_id
                    , user_id: UtilService.get_from_localstorage('user_info').email_id
                    , sku: osku.sku
                };

                ClaimService.add_override_sku($http, $q, override_sku)
                    .then(function (data) {
                        alert("Approved Part CODE.");
                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });

            }


        }

        $scope.insert_claim_details=function(){

                if(sku_array.length==0){
                    alert('Add Part CODE first');
                    return false;
                }

                var status="Verified & Approved";


                if(login_user_type!="schneider admin"){

                    angular.forEach(sku_array, function(item) {
                        if(item.status=="not-verified")
                        {
                            status="Schneider Verification Required";
                        }
                    });
                }

                appr_rej_send(status);
        }

        $scope.reject_claim_details=function(){

//            if(sku_array.length==0){
//                alert('Add sku first');
//                return false;
//            }
            var status="Rejected";
            appr_rej_send(status);
        }

        $scope.back_to_program=function(){

          window.location="#/main/program_management";
        }

        $scope.send_to_schneider=function(){

//            if(sku_array.length==0){
//                alert('Add Part CODE first');
//                return false;
//            }
            var status="Schneider Verification Required";

            angular.forEach(sku_array, function(item) {
                if(item.status=="not-verified")
                {
                    status="Schneider Verification Required";
                }
            });
            appr_rej_send(status);
        }

        function appr_rej_send(status){

            var date = new Date();
            var inv_date_track = [];
            inv_date_track= $scope.inv_date_list;

            var inv_date =  new Date($scope.invoice_date) ;
            var invoice_date='Invalid Date';

            if(prev_inv_date!=$scope.invoice_date){
                if(inv_date!='Invalid Date' ){
                    invoice_date=inv_date;
                    var inv_date_date =parseInt(inv_date.getDate());
                    var inv_date_month=parseInt(inv_date.getMonth())+1;
                    var inv_date_year=parseInt(inv_date.getFullYear());

                    if(inv_date_month>12 || inv_date_month<6){
                        alert ("Invoice date should be in between '01-06-2014' and '31-12-2014'");
                        return false;
                    }
                    else if(inv_date_year!=2014){
                        alert ("Invoice date should be in between '01-06-2014' and '31-12-2014'");
                        return false;
                    }
                    else if(inv_date_date>32 || inv_date_date<1){
                        alert ("Invoice date should be in between '01-06-2014' and '31-12-2014'");
                        return false;
                    }


                    if ( $scope.approval_comments == ""  || typeof $scope.approval_comments === 'undefined'){
                        alert ("Please enter comment");
                        return false;
                    }


                    var inv_date_tracking = {"user_id"   : UtilService.get_from_localstorage('user_info').email_id ,
                        "inv_date"  : $scope.invoice_date  ,
                        "track_date": date
                    };
                    inv_date_track.push(inv_date_tracking);
                }

            }





            var inv_amount = $scope.inv_detail.invoice_amount*1;
            var amount_track = [];
            amount_track= $scope.amount_list;


            var approval_comments=[];
            approval_comments= $scope.comment_list;
            var prev_inv_amount=0;
            angular.forEach(amount_track, function(item) {
                prev_inv_amount = item.invoice_amount*1;
            });
            var retVal=true;
            if(inv_amount!=prev_inv_amount){
                retVal   = confirm("Do you want to change the invoice amount?");
            }

            if(retVal){

                var user_comment_type="Schneider User";
                if(login_user_type=="schneider admin"){
                    user_comment_type="Schneider";
                }else if(login_user_type=="annectos admin"){
                    user_comment_type="Program Center";
                }

                var approve_comment={"comments"    :  $scope.approval_comments,
                                     "user_type"   :  user_comment_type,
                                     "user_id"     :  UtilService.get_from_localstorage('user_info').email_id,
                                     "comment_date": new Date()
                };

                approval_comments.push(approve_comment);



                var amount_tracking = {"user_id": UtilService.get_from_localstorage('user_info').email_id ,
                    "track_date": date,
                    "invoice_amount" :inv_amount};
                amount_track.push(amount_tracking);


                if(status=="Rejected" || status=="Schneider Verification Required"){
                    if(sku_array.length==0){
                        sku_array=[];
                    }
                }


                var  modified_by = UtilService.get_from_localstorage('user_info').email_id;
                var  modified_date= new Date();


                ClaimService.update_claim_details($http, $q, claim_id,inv_amount, sku_array,modified_by,modified_date, status , approval_comments, amount_track , inv_date_track, invoice_date)
                    .then(function (data) {

                        if(login_user_type=="schneider admin"){
                            trigger_email($scope.approval_comments,status,modified_by,$scope.inv_detail.invoice_number,inv_url);
                        }

                        if(status=="Verified & Approved"){
                            alert('Thanks!The Specific Invoice is Updated.This invoice (or part of the invoice)is now approved.');
                        }
                        else if(status=="Rejected")
                        {
                            alert("Thanks!The Specific Invoice is Updated.This invoice (or part of the invoice) is now rejected.");
                        }
                        else
                        {
                            alert("System can't approve this invoice because of missing information.This will be forwarded for Schneider Approval.");
                        }
                        $scope.isenable=true;
                        $scope.approval_comments="";


                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
            }



        }

        function trigger_email(approval_comments,status,modified_by,invoice_number,inv_url){

            var to_email=UtilService.get_to_email();

             var data={"template":{
                "name":"Schneider_Activity.html",
                "contact":[{
                    "head":{"emailid":to_email,"subject":"Schneider Activity"},
                    "body":{
                        "comment":approval_comments,
                        "status":status,
                        "email_id":modified_by,
                        "invoice_no":invoice_number,
                        "inv_url": inv_url
                    }
                }]

                }
             };

            ClaimService.trigger_email_srvc($http, $q, data);

        }


    });