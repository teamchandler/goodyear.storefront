'use strict';

angular.module('chandler.schneider')
  .controller('ClaimCtrl', function ($scope,$state,ClaimService,$http, $q, $upload,
                                    NotificationService, UtilService, MasterService) {


       $scope.invo_data="";


        $scope.numeric_only = function (event) {

//            alert(event);
//            alert(event.keyCode);
            // Allow only backspace and delete
            if (event.keyCode == 46 || event.keyCode == 8 ||event.keyCode ==190 ) {
                // let it happen, don't do anything
            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();

                }
            }
        };

       /**************Test Calender Panel*****************/
       $scope.today = function() {
           $scope.invo_data.invoice_date = new Date();
       };
        $scope.today();

        $scope.clear = function () {
            $scope.invo_data.invoice_date = null;
        };

        // Disable weekend selection
        $scope.disabled = function(date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        $scope.toggleMin = function() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.initDate = new Date('2016-15-20');
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];




        var get_upload_url = function(){

        }

        $scope.issaveenable=true;
      //  $scope.invo_data="";

//        $scope.$watch('invo_data.invoice_date', function(){
//
//            var start_date=UtilService.get_program_start_date();
//            var end_date= UtilService.get_program_end_date();
//            var inv_date=$scope.invo_data.invoice_date;
//
//            if(inv_date==start_date ||inv_date>start_date){
//
//                alert('ok');
//            }
//
//        });


        $scope.onFileSelect = function($files) {
            //$files: an array of files selected, each file has name, size, and type.
//            for (var i = 0; i < $files.length; i++) {

            var $file = $files[0];
//            console.log($file);
            $upload.upload({
                url: UtilService.get_api() +   "/aws/upload/file",
                file: $file,
                progress: function (e) {
                }
            }).then(function (data, status, headers, config) {
               // alert(data.data) ;
                $scope.issaveenable=false;
                console.log(data);
               // $scope.invo_data.supporting_doc="";
                $scope.invo_data.supporting_doc=data.data;
            });
//            MasterService.get_update_url($http, $q, $file.name)
//                .then(function (data) {
//                    console.log(data);
//                    MasterService.upload($http, $q, $file, $file.type, data)
//                        .then (function(response){
//                            console.log(response);
//                        },
//                        function () {
//                            //Display an error message
//                            $scope.error = error;
//
//                        });
//                },
//                function () {
//                    //Display an error message
//                    $scope.error = error;
//
//                });

//            }
        }
        function get_retailer_list(){

            ClaimService.get_retailer_list($http, $q)
                .then(function (data) {

                    $scope.distributor_list = data;

                    if (data[0].message != "no result found") {
                        $scope.not_end_of_list = true;
                    }
                    else {
                        $scope.s_data = data;
                        $scope.end_of_list = false;
                    }
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }


        $scope.save_invoice = function () {


            if (typeof $scope.invo_data === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }

           else if ($scope.invo_data.invoice_number == "" || typeof $scope.invo_data.invoice_number === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
           else if ($scope.invo_data.invoice_amount == "" || typeof $scope.invo_data.invoice_amount === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.invo_data.invoice_date == "" || typeof $scope.invo_data.invoice_date === 'undefined' ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ($scope.invo_data.retailer == "" || typeof $scope.invo_data.retailer === 'undefined'||$scope.invo_data.retailer == "Select Distributor" ){
                alert ("One of the required fields is missing. Please make sure that you have provided all required information!");
                return false;
            }
            else if ( $scope.invo_data.supporting_doc == ""  || typeof $scope.invo_data.supporting_doc === 'undefined'){
                alert ("Please select the file");
                return false;
            }

            var inv_date=$scope.invo_data.invoice_date;
//            var inv_date_array= inv_date.getDate();
            var inv_date_date =parseInt(inv_date.getDate());
            var inv_date_month=parseInt(inv_date.getMonth())+1;
            var inv_date_year=parseInt(inv_date.getFullYear());

            if(inv_date_month>12 || inv_date_month<6){
                alert ("Invoice date should be in between '01-06-2014' and '31-12-2014'");
                return false;
            }
            else if(inv_date_year!=2014){
                alert ("Invoice date should be in between '01-06-2014' and '31-12-2014'");
                return false;

            }
            else if(inv_date_date>32 || inv_date_date<1){
                alert ("Invoice date should be in between '01-06-2014' and '31-12-2014'");
                return false;

            }

//            if(inv_date_date<31){
//                inv_date_date=  inv_date_date+1;
//            }

//            var mod_inv_date=inv_date_month+"-"+inv_date_date+"-"+inv_date_year;
//            $scope.invo_data.invoice_date=mod_inv_date;

            $scope.invo_data.user_id=UtilService.get_from_localstorage('user_info').email_id;
            $scope.invo_data.company_name=UtilService.get_from_localstorage('user_info').company_name;
            $scope.invo_data.region=UtilService.get_from_localstorage('user_info').schneider_region;

            ClaimService.save_invoice($http, $q, $scope.invo_data)
                    .then(function (data) {
                      //  $scope.show_cond='s';
                        $scope.issaveenable=true;
                        alert(data);
                    },
                    function () {
                        //Display an error message
                        $scope.error = error;

                    });
        }


        $scope.clear_field = function () {
            $scope.invo_data="";
        }



        $scope.show_sku_list = function () {
            $scope.list_pop_show=true;

        }


        $scope.pop_up_close = function () {
            $scope.list_pop_show=false;
        }


         function search_sku_list() {
//            $scope.list_pop_show=false;


            var srch_sku="";
            if ( $scope.sku == ""  || typeof $scope.sku === 'undefined'){
                srch_sku='undefined';
            }else{
                srch_sku= $scope.sku;
            }

            ClaimService.search_sku_list($http, $q,srch_sku )
                .then(function (data) {

                    $scope.sku_list = data;

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });


        }

        var init = function(){
            UtilService.change_header($state.current.data.header_label);
            UtilService.select_menu($state.current.name);
            get_retailer_list();
            search_sku_list();
        }

        init();
  });
