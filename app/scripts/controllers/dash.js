/**
 * Created by developer6 on 10/21/2014.
 */


'use strict';

angular.module('chandler.schneider')
    .controller('DashCtrl', function ($scope,$state,$http,ClaimService, $q,NotificationService, UtilService, MasterService) {

        var init = function(){
            UtilService.change_header($state.current.data.header_label);
            UtilService.select_menu($state.current.name);

            get_top_si();
            get_top_si_by_region();
            get_si_by_region();
            get_normal_si_qualifier();
            get_special_si_qualifier();
        }
        var series_data=[];
        var si_data=[];

        function get_top_si(){

            ClaimService.get_top_si($http, $q)
                .then(function (data) {

                    var top_si_data=data;
                    var i=0;
                    angular.forEach(top_si_data, function(item) {
                        series_data.push( item.retailer) ;
                        i=i+1;
                        var si_data_obj={
                            "x" : i+". SI",
                            "y": [item.total_sale],
                            "tooltip" : "SI : "+ item.retailer +" , Total Sale : "+item.total_sale
                        };
                        si_data.push(si_data_obj);

                    });


               },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

        $scope.si_config = {
            title: 'Top 15 SI',
            tooltips: true,
            labels: false,
            mouseover: function() {},
            mouseout: function() {},
            click: function() {},
            legend: {
                display: true,
                position: 'right'
            }
        };

        $scope.si_data = {
//            series: ['Saleggggs', 'Income', 'Expense', 'Laptops', 'Keyboards'],
            data: si_data
        };

        var si_series_data=[];
        var si_data_by_region=[];


        function get_top_si_by_region(){

            ClaimService.get_top_si_by_region($http, $q)
                .then(function (data) {

                    var top_si_by_region=data;
                    var i=0;
                    angular.forEach(top_si_by_region, function(item) {
                        si_series_data.push( item.SI) ;
                        i=i+1;
                        var si_obj={
                            "x" : i+". SI",
                            "y": [item.total_sale],
                            "tooltip" : "SI : "+ item.SI +" , Total Sale : "+item.total_sale
                        };
                        si_data_by_region.push(si_obj);

                    });


                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

        $scope.top_si_config = {
            title: 'Top SI By Region',
            tooltips: true,
            labels: false,
            mouseover: function() {},
            mouseout: function() {},
            click: function() {},
            legend: {
                display: true,
                position: 'right'
            }
        };

        $scope.top_si_data = {
//            series: ['Saleggggs', 'Income', 'Expense', 'Laptops', 'Keyboards'],
            data: si_data_by_region
        };

        var si_series_by_region=[];
        var si_by_region=[];


        function get_si_by_region(){

            ClaimService.get_si_by_region($http, $q)
                .then(function (data) {
                    var i=0;
                    angular.forEach(data, function(item) {
                        si_series_by_region.push( item.region) ;
                        i=i+1;
                        var si_obj={
                            "x" :item.region,
                            "y": [item.total_sale],
                            "tooltip" : "Zone : "+ item.region +" , Total Sale : "+item.total_sale
                        };
                        si_by_region.push(si_obj);

                    });


                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

        $scope.si_config_region = {
            title: 'Total Sale By Region',
            tooltips: true,
            labels: false,
            mouseover: function() {},
            mouseout: function() {},
            click: function() {},
            legend: {
                display: true,
                position: 'right'
            }
        };

        $scope.si_data_region = {
//            series: ['Sales', 'Income', 'Expense', 'Laptops', 'Keyboards'],
            data: si_by_region
        };


        var si_normal_series=[];
        var si_normal=[];

        function get_normal_si_qualifier(){

            ClaimService.get_normal_si_qualifier($http, $q)
                .then(function (data) {
                    var i=0;
                    angular.forEach(data, function(item) {
                        si_normal_series.push( item._id) ;
                        i=i+1;
                        var si_obj={
                            "x" :item._id,
                            "y": [item.total_sale],
                            "tooltip" : "Zone : "+ item._id +" , Total Sale : "+item.total_sale
                        };
                        si_normal.push(si_obj);

                    });


                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

        $scope.si_config_normal = {
            title: 'Total Sale By Region',
            tooltips: true,
            labels: false,
            mouseover: function() {},
            mouseout: function() {},
            click: function() {},
            legend: {
                display: true,
                position: 'right'
            }
        };

        $scope.si_data_normal = {
//            series: ['Sales', 'Income', 'Expense', 'Laptops', 'Keyboards'],
            data: si_normal
        };


        var si_special_series=[];
        var si_special=[];

        function get_special_si_qualifier(){

            ClaimService.get_special_si_qualifier($http, $q)
                .then(function (data) {
                    var i=0;
                    angular.forEach(data, function(item) {
                        si_special_series.push( item._id) ;
                        i=i+1;
                        var si_obj={
                            "x" :item._id,
                            "y": [item.total_sale],
                            "tooltip" : "Zone : "+ item._id +" , Total Sale : "+item.total_sale
                        };
                        si_special.push(si_obj);

                    });


                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

        $scope.si_config_special = {
            title: 'Total Sale By Region',
            tooltips: true,
            labels: false,
            mouseover: function() {},
            mouseout: function() {},
            click: function() {},
            legend: {
                display: true,
                position: 'right'
            }
        };

        $scope.si_data_special = {
//            series: ['Sales', 'Income', 'Expense', 'Laptops', 'Keyboards'],
            data: si_special
        };


        init();











    });
