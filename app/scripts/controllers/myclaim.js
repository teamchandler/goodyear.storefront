/**
 * Created by developer6 on 6/16/2014.
 */

'use strict';

angular.module('chandler.schneider')
    .controller('myclaimCtrl', function ($scope,$state,ClaimService,$http, $q, $upload,
                                       NotificationService, UtilService, MasterService) {


        $scope.pop_show=false;

        $scope.pop_up_close=function(){
            $scope.pop_show=false;

        }

//        var sku_staus="verified";

//        $scope.get_lay_out=function(){
//            var part_class="";
//            if(sku_staus=="verified"){
//                part_class= "part_no_verify_status";
//            }
//            else{
//                part_class="part_no_not_verify_status";
//            }
//            return part_class;
//        }
        $scope.invoice_details_by_number=function(claim_id){
            $scope.comment_list=[];
            $scope.pop_show=true;
            ClaimService.invoice_details_by_number($http, $q, claim_id)
                .then(function (data) {

                    $scope.comment_list=data[0].approval_comments;
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }



        init();
        function init(){
            UtilService.change_header($state.current.data.header_label);
            UtilService.select_menu($state.current.name);
            
            ClaimService.get_invoice_by_user_name($http, $q, UtilService.get_from_localstorage('user_info').email_id)
                .then(function (data) {

                    $scope.invoice = data;
//                    var date_time = $scope.invoice[0].invoice_date;
//
//                    var date = new Date(date_time).toUTCString();
//                    $scope.invoice[0].invoice_date = date;
//
//
//                    if (data[0].message != "no result found") {
//                        $scope.not_end_of_list = true;
//                    }
//                    else {
//                        $scope.region_list = data;
//                        $scope.end_of_list = false;
//                    }

                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });

        }

    });