'use strict';

angular.module('chandler.schneider')
  .controller('SeepCtrl', function ($scope, $state,
                                    NotificationService, UtilService) {
        var init = function(){
            UtilService.change_header($state.current.data.header_label);
            UtilService.select_menu($state.current.name);
        }
        init();
  });
