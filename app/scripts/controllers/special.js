'use strict';

angular.module('chandler.schneider')
  .controller('SpecialCtrl', function ($scope,$state,
                                       NotificationService,$stateParams, UtilService) {


        var special_type=$stateParams.special_type;

        if(special_type=="ebw"){
            $scope.special_ebw=true;
        }
        else {
            $scope.special_ids=true;
        }

        var init = function(){
            UtilService.change_header($state.current.data.header_label);
            UtilService.select_menu($state.current.name);
        }
        init();
  });
