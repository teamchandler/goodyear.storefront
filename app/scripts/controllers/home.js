'use strict';

angular.module('chandler.schneider')
  .controller('HomeCtrl', function ($scope, $state,
                                    NotificationService, UtilService) {


        var init = function(){

            UtilService.change_header($state.current.data.header_label);
            UtilService.select_menu($state.current.name);
            $scope.user = UtilService.get_from_localstorage("user_info");
            if($scope.user.credit_point_normal == null)
            {
                $scope.user.credit_point_normal  = 0;
            }
            else if($scope.user.credit_point_special == null)
            {
                $scope.user.credit_point_special  = 0;

            }

        }
        init();
  });
