'use strict';

angular.module('chandler.schneider')
  .controller('ChangePasswordCtrl', function (  $scope, $state,$http, $q,
                                                UtilService, MasterService
                                            ) {


        $scope.change_password = function(){
            var credential={};
            credential.company = UtilService.get_company();
            credential.email_id = $scope.email_id;
            credential.old_password = $scope.old_password;
            credential.new_password = $scope.new_password;
            MasterService.change_password($http, $q, credential)
                .then
                (
                function (data) {

                    if (data[0][0].msg ==='password updated'){
                        alert("You have successfully changed your password. Please login with new credentials now.");
                        UtilService.set_to_localstorage("user_info", "");
                        $state.go("login.login");
                    }
                    else{

                        UtilService.set_to_localstorage("user_info", "");
                        alert(data[0][0].msg);
                    }
                },

                function () {
                    //Display an error message
                    $scope.error = error;
                }
            );

        }

        init();

        function init()
        {
            UtilService.change_header($state.current.data.header_label);
            UtilService.select_menu($state.current.name);
            $scope.username = "";
            $scope.old_password = "";
            $scope.new_password = "";

        };
  });
