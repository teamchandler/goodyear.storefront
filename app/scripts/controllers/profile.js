'use strict';

angular.module('chandler.schneider')
  .controller('ProfileCtrl', function ($scope, $state,$http, $q,
                                       NotificationService, UtilService, MasterService) {

//        $(function(){
//            $('.datepicker').datepicker({
//                format: 'mm-dd-yyyy'
//            });
//        })


        var init = function(){
            UtilService.change_header($state.current.data.header_label);
            UtilService.select_menu($state.current.name);
            $scope.state_list = UtilService.get_state_list();

            get_profile_info();


        }
        $scope.select_address = function (selected_address) {
            populate_selected_address(selected_address);
        }

        var selected_address = {};

        var populate_selected_address = function (address) {

            selected_address.first_name = address.first_name;
            selected_address.last_name = address.last_name;
            selected_address.street = address.street;
            selected_address.address = address.address;
            selected_address.city = address.city;
            selected_address.state = address.state;
            selected_address.pincode = address.pincode;
            selected_address.mobile_no = address.mobile_no;
            selected_address.zipcode = address.zipcode;
         //   selected_address.email_id = localStorageService.get('user_info').email_id;
         //   selected_address.store = utilService.get_store().toLowerCase();
            $scope.selected_address = selected_address;
        }

        function get_profile_info(){

           var user_data= UtilService.get_from_localstorage('user_info');

            MasterService.get_profile_info($http, $q, user_data.email_id)
                .then
            (
                function (data) {
                    console.log(data[0][0]);
                    $scope.user_data=data[0][0];
                    $scope.user_shipping_list = data[0][0];

                    if(data[0][0].comp_found_date=="00-00-0000"){
                        $scope.user_data.cmp_found_dt="";
                    }
                    else
                    {
                        $scope.user_data.cmp_found_dt=data[0][0].comp_found_date;
                    }
                    if(data[0][0].user_dob=="00-00-0000"){
                        $scope.user_data.user_date_of_birth="";
                    }
                    else{
                        $scope.user_data.user_date_of_birth=data[0][0].user_dob;
                    }
                },

                function () {
                    //Display an error message
                    $scope.error = error;
                }
            );
        }


        $scope.save_user_basic_info = function () {

            if ($scope.user_data.first_name == "" || typeof $scope.user_data.first_name === 'undefined' ){
                alert ("Please enter first name");
                return false;
            }
            else if ($scope.user_data.last_name == "" || typeof $scope.user_data.last_name === 'undefined'){
                alert ("Please enter last name");
                return false;
            }
            else if ($scope.user_data.user_date_of_birth == "" || typeof $scope.user_data.user_date_of_birth === 'undefined'){
                alert ("Please enter date of birth");
                return false;
            }
            var dob=$scope.user_data.user_date_of_birth;

            if(dob!=null && dob!=""){
                var dob_date_array= dob.split('-');
                var dob_date_date =parseInt(dob_date_array[0]);
                var dob_date_month=parseInt(dob_date_array[1]);
                var dob_date_year=parseInt(dob_date_array[2]);
                $scope.user_data.user_dob=dob_date_year+"-"+dob_date_month+"-"+dob_date_date;
            }
            else{
                $scope.user_data.user_dob="";
            }

            var cfd=$scope.user_data.cmp_found_dt;

            if(cfd!=null && cfd!=""){
                var cfd_date_array= cfd.split('-');
                var cfd_date_date =parseInt(cfd_date_array[0]);
                var cfd_date_month=parseInt(cfd_date_array[1]);
                var cfd_date_year=parseInt(cfd_date_array[2]);
                $scope.user_data.comp_found_date=cfd_date_year+"-"+cfd_date_month+"-"+cfd_date_date;
            }
            else{
                $scope.user_data.comp_found_date="";
            }


            MasterService.save_user_basic_info($http, $q, $scope.user_data)
                .then(function (data) {
                    alert('Profile information updated successfully');
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }


        $scope.update_user_address = function () {

            $scope.selected_address.email_id=UtilService.get_from_localstorage('user_info').email_id;
            $scope.selected_address.gender=$scope.user_data.gender;
          //  $scope.selected_address.mobile_no=$scope.user_data.mobile_no;
            $scope.selected_address.office_no="0000";
            $scope.selected_address.country="India";
            $scope.selected_address.company=UtilService.get_company();


            if ($scope.selected_address.first_name == "" || typeof $scope.selected_address.first_name === 'undefined' ){
                alert ("Please enter first name");
                return false;
            }
            else if ($scope.selected_address.last_name == "" || typeof $scope.selected_address.last_name === 'undefined'){
                alert ("Please enter last name");
                return false;
            }
            else if ($scope.selected_address.address == "" || typeof $scope.selected_address.address === 'undefined'){
                alert ("Please enter address");
                return false;
            }
            else if ($scope.selected_address.street == "" || typeof $scope.selected_address.street === 'undefined'){
                alert ("Please enter landmark");
                return false;
            }
            else if ($scope.selected_address.city == "" || typeof $scope.selected_address.city === 'undefined'){
                alert ("Please enter city");
                return false;
            }
            else if ($scope.selected_address.state == "" || typeof $scope.selected_address.state === 'undefined'){
                alert ("Please enter state");
                return false;
            }
            else if ($scope.selected_address.zipcode == "" || typeof $scope.selected_address.zipcode === 'undefined'){
                alert ("Please enter pincode");
                return false;
            }
            else if ($scope.selected_address.mobile_no == "" || typeof $scope.selected_address.mobile_no === 'undefined'){
                alert ("Please enter phone number");
                return false;
            }

            MasterService.update_user_address($http, $q, $scope.selected_address)
                .then(function (data) {
                    alert('Profile information updated successfully');
                },
                function () {
                    //Display an error message
                    $scope.error = error;

                });
        }

        init();
  });
