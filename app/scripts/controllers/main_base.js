'use strict';

angular.module('chandler.schneider')
  .controller('MainBaseCtrl', function ($scope, $state,
                                        UtilService, NotificationService)
    {
        var check_user = function(){
            var u = UtilService.get_from_localstorage('user_info');
            //console.log(u);
            if ((u == "") || (u == null)) {
                // send to login
                $state.go('login.login');

            }
        }

        var init = function(){
            check_user();
        }
        init();
  });
