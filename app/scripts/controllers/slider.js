'use strict';

angular.module('chandler.schneider')
  .controller('SliderCtrl', function ($scope, $state, UtilService) {



        var init = function(){
            var main_page_slider = UtilService.get_main_page_slider();
            $scope.myInterval = 2000;
            var slides = $scope.slides = [];
            for (var i=0; i<main_page_slider.length; i++){
                    var newWidth = 600 + slides.length;

                        slides.push({
                            image: main_page_slider[i].link,
                            href : main_page_slider[i].href
                        });

                }

        }
        init();

  });
