'use strict';

angular.module('chandler.schneider', [
    'ui.router',
    'LocalStorageModule',
    'angularFileUpload',
    'ui.bootstrap',
    'angularCharts'
])
//    .run(function(UtilService, $http, $q){
//        UtilService.load_config($http, $q)
//            .then
//    })

    .run(function ($rootScope, $location) {
        $rootScope.$on('$routeChangeSuccess', function () {
            ga('send', 'pageview', $location.path());
        });
    })

    .config(function ($stateProvider, $urlRouterProvider) {


        $urlRouterProvider.otherwise('/main/home');

        $stateProvider
            .state('login', {
                url: "/login",
                abstract: true,
                templateUrl: "views/login_base.html",
                controller : "LoginBaseCtrl"
            })
            .state('login.login', {
                url: '/login',
                templateUrl: 'views/login_login.html',
                data: {
                    "header_label" : "Please Sign In"
                },
                controller: 'LoginLoginCtrl'
            })
            // the pet tab has its own child nav-view and history
            .state('login.forget_password', {
                url: '/forget_password',
                templateUrl: 'views/forget_password.html',
                controller: 'ForgetPasswordCtrl',
                data: {
                    "header_label" : "Forget Password"
                }
            })
            .state('login.registration', {
                url: '/registration',
                templateUrl: 'views/registration.html',
                controller: 'RegistrationCtrl',
                data: {
                    "header_label" : "Register"
                }
            })
            .state('login.change_password', {
                url: '/change_password',
                templateUrl: 'views/change_password.html',
                controller: 'ChangePasswordCtrl',
                data: {
                    "header_label" : "Change Password"
                }
            })
            .state('login.support', {
                url: '/support',
                templateUrl: 'views/support.html',
                controller: 'support_controller',
                data: {
                    "header_label" : "Support Numbers"
                }
            })


            .state('main', {
                url: "/main",
                abstract: true,
                templateUrl: "views/main_base.html",
                controller : "MainBaseCtrl"
            })
            .state('main.home', {
                url: '/home',
                templateUrl: 'views/home.html',
                controller: 'HomeCtrl',
                data: {
                    "header_label" : "Home"
                }
            })
            .state('main.seep', {
                url: '/seep',
                templateUrl: 'views/seep.html',
                controller: 'SeepCtrl',
                data: {
                    "header_label" : "SEEP"
                }
            })

            .state('main.claim', {
                url: '/claim',
                templateUrl: 'views/claim.html',
                controller: 'ClaimCtrl',
                data: {
                    "header_label" : "Claim Entry"
                }
            })

            .state('main.contact', {
                url: '/contact',
                templateUrl: 'views/contact.html',
                controller: 'ContactCtrl',
                data: {
                    "header_label" : "Contact Us"
                }
            })

            .state('main.profile', {
                url: '/profile',
                templateUrl: 'views/profile.html',
                controller: 'ProfileCtrl',
                data: {
                    "header_label" : "My Profile"
                }
            })

            .state('main.management', {
                url: '/program_management',
                templateUrl: 'views/program.html',
                controller: 'programCtrl',
                data: {
                    "header_label" : "Program Admin"
                }
            })

            .state('main.redemption', {
                url: '/redemption',
                templateUrl: 'views/redemption.html',
                controller: 'redemptionCtrl',
                data: {
                    "header_label" : "Redemption"
                }
            })

            .state('main.redemption_more', {
                url: '/redemption_more/:order_id/:status/:order_date/:points',
                templateUrl: 'views/redemption_more.html',
                controller: 'redemptionmoreCtrl',
                data: {
                    "header_label" : "Redemption More"
                }
            })


            .state('main.program_details', {
                url: '/program_details/:claim_id',
                templateUrl: 'views/program_details.html',
                controller: 'programdetlCtrl',
                data: {
                    "header_label" : "Program Details"
                }
            })

            .state('main.myclaims', {
                url: '/my_claims',
                templateUrl: 'views/myclaim.html',
                controller: 'myclaimCtrl',
                data: {
                    "header_label" : "My Claims"
                }
            })

            .state('main.dash', {
                url: '/dash',
                templateUrl: 'views/dash.html',
                controller: 'DashCtrl',
                data: {
                    "header_label" : "Dash Board"
                }
            })

            .state('main.special', {
                url: '/special/:special_type',
                templateUrl: 'views/special.html',
                controller: 'SpecialCtrl',
                data: {
                    "header_label" : "Special"
                }
            })
        ;
  });
