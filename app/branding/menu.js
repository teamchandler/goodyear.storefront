/**
 * Created by rahulguha on 11/06/14.
 */
var menu = [
    {'display_name': 'Home', 'link': '#/main/home', 'state': 'main.home', 'admin': 0},
    {'display_name': 'PROFILE', 'link': '#/main/profile', 'state': 'main.profile', 'admin': 0},
    //{'display_name': 'SEEP', 'link': '#/main/seep', 'state': 'main.seep', 'admin': 0},
    {'display_name': 'Claim Entry', 'link': '#/main/claim', 'state': 'main.claim', 'admin': 0},
    {'display_name': 'My Claims', 'link': '#/main/my_claims', 'state': 'main.my_claims', 'admin': 0},
    {'display_name': 'Redemption', 'link': '#/main/redemption', 'state': 'main.redemption', 'admin': 0},
    {'display_name': 'Reward Gallery', 'link': '/rewards', 'state': '', 'admin': 0},
    {'display_name': 'Contact', 'link': '#/main/contact', 'state': 'main.contact', 'admin': 0},
    {'display_name': 'Prog Adm', 'link': '#/main/program_management', 'state': 'main.management', 'admin': 1},
    {'display_name': 'Special', 'link': '#/main/special/ids', 'state': 'main.special', 'admin': 0},
    {'display_name': 'Change Password', 'link': '#/login/change_password', 'state': 'login.change_password', 'admin': 0},
    {'display_name': 'Dash', 'link': '#/main/dash', 'state': 'main.dash', 'admin': 1},
    {'display_name': 'Logout', 'link': '#/login/login#login_box', 'state': 'login.login', 'admin': 0}

];

var default_menu = "main.home";
