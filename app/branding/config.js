/**
 * Created by rahulguha on 11/06/14.
 */



var api="http://seep-digilink.in:20010";
//var api = "http://localhost:20010";

var company ='goodyear';
var default_title ='Schneider Claim Portal';

var prog_start_dt ='06-01-2014';
var prog_end_dt ='12-31-2014';

var to_email="info@seep-digilink.in";

var annectos_program_admin = ["annectos_program_admin@annectos.in","seema_program_admin@annectos.in","naveen_program_admin@annectos.in", "chandler@annectos.in"];
var schneider_program_admin = ["schneider_program_admin@annectos.in", "chandler@annectos.in"];

var encryption_phrase = "June2014";
var state_list = [
    { id: "AP", text: "Andhra Pradesh" },
    { id: "AR", text: "Arunachal Pradesh" },
    { id: "AS", text: "Assam" },
    { id: "BR", text: "Bihar" },
    { id: "CH", text: "Chhattisgarh" },
    { id: "DL", text: "Delhi" },
    { id: "GA", text: "Goa" },
    { id: "GJ", text: "Gujarat" },
    { id: "HR", text: "Haryana" },
    { id: "HP", text: "Himachal Pradesh" },
    { id: "JK", text: "Jammu and Kashmir" },
    { id: "KA", text: "Karnataka" },
    { id: "KL", text: "Kerala" },
    { id: "MP", text: "Madhya Pradesh" },
    { id: "MH", text: "Maharashtra" },
    { id: "MN", text: "Manipur" },
    { id: "ML", text: "Meghalaya" },
    { id: "MZ", text: "Mizoram" },
    { id: "NL", text: "Nagaland" },
    { id: "OR", text: "Orissa" },
    { id: "Pb", text: "Punjab" },
    { id: "UP", text: "Uttar Preadesh" },
    { id: "UT", text: "Uttaranchal" },
    { id: "RJ", text: "Rajasthan" },
    { id: "SK", text: "Sikkim" },
    { id: "TN", text: "Tamil Nadu" },
    { id: "TR", text: "Tripura" },
    { id: "WB", text: "West Bengal" }];

var user_type="schneider user";