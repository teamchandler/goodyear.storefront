/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 6/10/13
 * Time: 10:12 AM
 * To change this template use File | Settings | File Templates.
 */
/// <reference path="../Scripts/angular-1.1.4.js" />

/*#######################################################################

 Rahul Guha

 Normally like to break AngularJS apps into the following folder structure
 at a minimum:

 /app
 /controllers
 /directives
 /services
 /partials
 /views
********************CHECKOUT APP
 #######################################################################*/

var app = angular.module('ECOMM.login', ['ngRoute', 'LocalStorageModule']);
app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]);
//This configures the routes and associates each route with a view and a controller
app.config(function ($routeProvider) {
    $routeProvider

         
        .when('/registration',
        {
            controller: 'registrationController',
            templateUrl: '../login/app/partials/Registration.html'
        })
        .when('/login',
        {
            controller: 'loginController',
            templateUrl: '../login/app/partials/login.html'
        })

        .when('/forgot_password',
        {
            controller: 'forgotpwdController',
            templateUrl: '../login/app/partials/forgot_password.html'
        })

        .when('/message',
        {
            controller: 'MessageController',
            templateUrl: '../login/app/partials/message.html'
        })


       
        .when('/activation/:id/:cid',
        {
            controller: 'ActivationController',
            templateUrl: '../login/app/partials/accountActivation.html'
        })
        .when('/reset_password/:id/:cid',
        {
            controller: 'reset_passwordController',
            templateUrl: '../login/app/partials/reset_password.html'
        })

        .otherwise({ redirectTo: '/home' });
});