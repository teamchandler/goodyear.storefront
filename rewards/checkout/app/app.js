/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 6/10/13
 * Time: 10:12 AM
 * To change this template use File | Settings | File Templates.
 */
/// <reference path="../Scripts/angular-1.1.4.js" />

/*#######################################################################

 Rahul Guha

 Normally like to break AngularJS apps into the following folder structure
 at a minimum:

 /app
 /controllers
 /directives
 /services
 /partials
 /views
********************CHECKOUT APP
 #######################################################################*/

var app = angular.module('ECOMM.checkout', ['ngRoute','ui.bootstrap', 'LocalStorageModule']);
app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]);
//This configures the routes and associates each route with a view and a controller
app.config(function ($routeProvider) {
    $routeProvider
        .when('/ship',
        {
            controller: 'ShipController',
            templateUrl: '../checkout/app/partials/ship.html'
        })
        .when('/cc/:order_id',
        {
            controller: 'CcController',
            templateUrl: '../checkout/app/partials/cc.html'
        })
        .when('/nb/:order_id',
        {
            controller: 'NbController',
            templateUrl: '../checkout/app/partials/nb.html'
        })

        .when('/pp/:order_id',
        {
            controller: 'PpController',
            templateUrl: '../checkout/app/partials/pp.html'
        })
        .when('/dc/:order_id',
        {
            controller: 'DcController',
            templateUrl: '../checkout/app/partials/dc.html'
        })
        .when('/exp/:order_id',
        {
            controller: 'express_popupController',
            templateUrl: '../checkout/app/partials/express_pop_up.html'
        })
//        .when('/cart',
//        {
//            controller: 'cartController',
//            templateUrl: '/app/partials/cart_page.html'
//        })
//        .when('/checkout',
//        {
//            controller: 'checkoutController',
//            templateUrl: '/app/partials/checkout.html'
//        })
        .when('/order',
        {
            controller: 'orderController',
            templateUrl: '/rewards/app/partials/order.html'
        })

        .when('/special/:catId',
        {
            controller: 'specialController',
            templateUrl: '/rewards/app/partials/special.html'
        })

        .when('/newarrivals',
        {
            controller: 'newarrivalsController',
            templateUrl: '/rewards/app/partials/newarrivals.html'
        })

        .when('/premium',
        {
            controller: 'premiumController',
            templateUrl: '/rewards/app/partials/premium.html'
        })

        .when('/express',
        {
            controller: 'expressController',
            templateUrl: '/rewards/app/partials/express.html'
        })
        .when('/cat/:catId',
        {
            controller: 'CatController',
            templateUrl: '/rewards/app/partials/cat.html'
        })


        .otherwise({ redirectTo: '/home' });
});