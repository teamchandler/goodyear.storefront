/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 6/10/13
 * Time: 10:15 AM
 * To change this template use File | Settings | File Templates.
 */
app.controller('ShipController', function ($scope, $http, $q, $location, $window,
                                           shipService, localStorageService, cartService, utilService ) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    function check_login (){
        if (localStorageService.get('user_info') == null){
            $window.location.href = root_url + "/index.html#/login";
        }
        else
        {
            $window.location.href = root_url + "/home.html";
        }
    }
    $scope.select_address = function(selected_address){
        populate_selected_address(selected_address);
    }
    var get_user_shipping = function(){
        $scope.loading = true;
        var e = localStorageService.get('user_info');
        var u;
        if (e.hasOwnProperty('email_id')){
            u = e.email_id;
        }
        else {
            u= e.user_id;
        }
//        shipService.get_user_shipping_list($http, $q, localStorageService.get('user_info').email_id).then(function(data){
        shipService.get_user_shipping_list($http, $q, u).then(function(data){
                $scope.user_shipping_list =  data;
                //console.log($scope.user_shipping_list) ;
                $scope.loading = false;
            },
            function(){
                //Display an error message
                $scope.error= error;
                $scope.loading = false;
            });
      }
    $scope.save_data = function(){

        //$location.path("/exp/769");

        if (selected_address.name == ""){
            alert ("Please enter Shipping Address Name")
        }else{
            if (selected_address.address == ""){
                alert ("Please enter Shipping Address")
            }
            else{
                if (selected_address.city == ""){
                    alert ("Please enter Shipping Address City")
                }
                else {
                    if (selected_address.pincode == ""){
                        alert ("Please enter Shipping Address Postal Code")
                    }
                    else {
                        if (selected_address.mobile_number== ""){
                            alert ("Please enter Mobile Number")
                        }else{
                            var ship_data = [];
                            ship_data.push(cartService.get_Cart());
                            ship_data.push(selected_address);

                            shipService.save_shipping_page($http, $q , selected_address).then (function(data){
                                localStorageService.set('selected_ship_address', selected_address);
                                // check for express
                                //console.log(cartService.get_Cart())    ;
                                var c = cartService.get_Cart();
                                var express = false;
                                for (i =0; i < c.length; i++){
                                    if (c[i].express){
                                        express = true;
                                        break;
                                    }
                                }
                                if ( express){
                                    // have express - send to express_popup
                                    if (utilService.get_express_shipping() == 1){
                                        $location.path("/exp/" + data.toString());
                                    }
                                    else{
                                        $location.path("/pp/" + data.toString());
                                    }
                                }
                                else {
                                    // no express - send directly to payment page
                                    $location.path("/pp/" + data.toString());
                                }
                            });
                        }
                    }
                }
            }
        }

    }
    var populate_selected_address = function (address){
        selected_address.id = address.id;
        selected_address.name = address.name;
        selected_address.shipping_name = address.shipping_name;
        selected_address.address= address.address;
        selected_address.city= address.city;
        selected_address.state = address.state;
        selected_address.pincode = address.pincode;
        selected_address.mobile_number= address.mobile_number;
        var e = localStorageService.get('user_info');
        var u;
        if (e.hasOwnProperty('email_id')){
            u = e.email_id;
        }
        else {
            u= e.user_id;
        }
        selected_address.user_id = u;
        //selected_address.store = store_signature;
        selected_address.store = utilService.get_store().toLowerCase();
        $scope.selected_address = selected_address;
    }
    function init() {
        $scope.loading = true;
        // load state and country
        $scope.state_list = shipService.get_state_list();
        $scope.country_list = shipService.get_country_list();
        get_user_shipping(); // populates  $scope.user_shipping_list
        var e = localStorageService.get('user_info');
        var u;
        if (e.hasOwnProperty('email_id')){
            u = e.email_id;
        }
        else {
            u= e.user_id;
        }
        populate_selected_address({
            id : 0, name : "", shipping_name : "",
            address: "", city: "", state : "",
            pincode : "", mobile_number : "", user_id : u,
            store : utilService.get_store().toLowerCase() //store_signature
        });
        $scope.loading = false;

    }
    var selected_address = {};
console.log (utilService.get_store().toLowerCase());
    init();

});