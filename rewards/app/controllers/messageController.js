﻿
app.controller('MessageController', function ($scope, $routeParams, $location, $http, $q, messageService, registrationService, localStorageService) {

    $scope.showButton = function () {
        return true;
    }

    $scope.showLoginButton = function () {
        return true;
    }

    Valid_Message = localStorageService.get('message');


    if (Valid_Message == "Your Registration Has Been Successfully Completed") {
        //$location.path("#/registration_success");
        //alert("Registration is Successfull now!  Please check your email for activation of account.");
        Valid_Message="Registration is Successfull now!  Please check your email for activation of account.";
        $scope.message = Valid_Message;
        localStorageService.set("user_info", "");
        localStorageService.set("reg_user", "");
        //$location.path("#/home");
    }
    else if (Valid_Message == "Your Account Activation Is Pending") {

        $scope.message = Valid_Message;
        localStorageService.set("user_info", "");

        $scope.showButton = function () {
            return false;
        }
    }
    else if (Valid_Message == "This Email Is Already Registered") {

        $scope.message = Valid_Message;
        localStorageService.set("user_info", "");

        $scope.showLoginButton = function () {
            return false;
        }
       
        
    }
    else {
        localStorageService.set("reg_user", "");
        $scope.message = Valid_Message;
    }

    $scope.SendLogin = function () {
      
        $location.path("/login");
    }
    $scope.ResendActCode = function () {

    Reg_User_Info = localStorageService.get('reg_user');

        var returnval = '';
        //returnval = registrationService.UserRegistration($http, $q, $scope.registration);
        registrationService.ResendActCode($http, $q, Reg_User_Info).then(function (data) {
            console.log(data + " -- " + data.toString().toLowerCase() + " -- " + "success");

            if (JSON.parse(data.toString()) == "Invalid Email") {
                $scope.message = JSON.parse(data.toString());
            }
            else {
                $scope.message = JSON.parse(data.toString());
                $scope.showButton = function () {
                    return true;
                }
            }

        },
            function () {
                //Display an error message
                $scope.error = error;
            });
    };


});
