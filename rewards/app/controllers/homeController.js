/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 21/7/13
 * Time: 2:02 PM
 * To change this template use File | Settings | File Templates.
 */
app.controller('HomeController', function ($scope, $location,localStorageService, $window) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example below
    function check_login (){
        if (localStorageService.get('user_info') == null){
            //$window.location.href = root_url + "/index.html#/login";
            $location.path("/login");
        }
        else
        {
            $window.location.href = root_url + "/home.html";
        }
    }
    function init() {
        console.log(root_url + "/home.html");
        $window.location.href = root_url + "/home.html";
    }
    init();

});