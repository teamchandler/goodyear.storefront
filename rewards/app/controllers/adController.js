/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 13/7/13
 * Time: 5:55 AM
 * To change this template use File | Settings | File Templates.
 */


app.controller('adController', function ($scope, $location, $http, $q,  $window,
                                         $routeParams,  utilService, adService, localStorageService, menuService) {

    //I like to have an init() for controllers that need to perform some initialization. Keeps things in
    //one place...not required though especially in the simple example bel   ow

    var slides = $scope.slides = [];
    var ads;// = adService.get_ads();
    var level1_banners;
    var refresh_date = new XDate();
    var cat =  $routeParams.cat;
    function loadPageVar (sVar) {
        return ($window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
    }

// Would alert the value of QueryString-variable called name
    cat = loadPageVar("cat");
    function get_home_banner(local){
        //console.log("fetching home banner from server");
        if (!local){
            adService.get_home_banner($http, $q).then(function(data){
                    ads = data;
                    localStorageService.set("home_banner_ads", ads);
                    localStorageService.add("ad_refresh",refresh_date.toString("MMM d, yyyy"));  //new XDate());
                    for (i=0; i<ads.length; i++)
                    {
                        $scope.addSlide(ads[i].link, ads[i].href)
                    }
                },
                function(){
                    //Display an error message
                    $scope.error= error;
                });
        }
        else {
            ads =  utilService.get_landing_page_scroller();
            localStorageService.set("home_banner_ads", ads);
            for (i=0; i<ads.length; i++)
            {
                $scope.addSlide(ads[i].link, ads[i].href)
            }
        }
    }
    function get_level1_banner(){
        adService.get_level1_banner($http, $q).then(function(data){
                level1_banners = data;
                localStorageService.set("level1_banners", level1_banners);
                localStorageService.add("ad_refresh",refresh_date.toString("MMM d, yyyy"));  //new XDate());
                var cat1_ads = _.select(level1_banners, function(b){ return b.id == cat;});
                for (i=0; i<cat1_ads.length; i++)
                {
                    $scope.addSlide(cat1_ads[i].link, cat1_ads[i].href)
                }
            },
            function(){
                //Display an error message
                $scope.error= error;
            });
    }
    function init() {

        $scope.myInterval = 3000;
        //ads = adService.get_ads();
        //utilService.local_banners_exist = false;
        if (utilService.local_banners_exist()){
            // banners are picked up from local
            if (cat == ""){  // for home page
                ads =  utilService.get_landing_page_scroller();
                localStorageService.set("home_banner_ads", ads);
                for (i=0; i<ads.length; i++)
                {
                    $scope.addSlide(ads[i].link, ads[i].href)
                }
            }
            else{               // for cat1 page
                level1_banners =  utilService.get_l1_banner();
                var cat1_ads = _.select(level1_banners, function(b){ return b.id == cat;});
                for (i=0; i<cat1_ads.length; i++)
                {
                    $scope.addSlide(cat1_ads[i].link, cat1_ads[i].href)
                }
            }
        }
        else {
            // banner is picked up from server - default set of banners
            var expiry_status;
            var last_download_date;

            var tmp_menu =localStorageService.get("ad_refresh");
            if (localStorageService.get("ad_refresh")===null)
            {
                last_download_date = ""
            }
            else{
                last_download_date =  localStorageService.get("ad_refresh");
            }

            menuService.get_menu_expiry($http, $q, last_download_date).then(function(data){
                    expiry_status = data;
                    //console.log(expiry_status);
                    if (cat == ""){  // for home page
                        if (localStorageService.get("home_banner_ads") == null){

                            get_home_banner();
                        }
                        else{
                            if (expiry_status > 0 ){
                                get_home_banner();
                            }
                            else {
                                ads =  localStorageService.get("home_banner_ads");
                                for (i=0; i<ads.length; i++)
                                {
                                    $scope.addSlide(ads[i].link, ads[i].href)
                                }
                            }
                        }
                    }
                    else{               // for cat1 page
                        if (localStorageService.get("level1_banners") == null){
                            get_level1_banner();
                        }
                        else {
                            if (expiry_status > 0 ){
                                get_level1_banner();
                            }
                            else {
                                level1_banners =  localStorageService.get("level1_banners");
                                var cat1_ads = _.select(level1_banners, function(b){ return b.id == cat;});
                                for (i=0; i<cat1_ads.length; i++)
                                {
                                    $scope.addSlide(cat1_ads[i].link, cat1_ads[i].href)
                                }
                            }
                            //level1_banners =  localStorageService.get("level1_banners");
                            //console.log(level1_banners);
                        }
                        console.log(level1_banners);


                    }

                },
                function(){
                    $scope.error= error;
                })

        }

    }

    $scope.addSlide = function(img_link,  img_href) {
        var newWidth = 300;// + ((slides.length + (25 * slides.length)) % 150);
        slides.push({
            image: img_link ,  //'http://placekitten.com/' + newWidth + '/200'
            href: img_href

        });
    };

    //console.log (cat);
    //console.log(cat + "_static.html");
    if (cat == ""){
        $scope.template = { name: 'landing_page_static', url:  '/rewards/Content/ad/landing_page_static_ad.html'};//$scope.templates[0];
    }
    else{
        var static_url =cat + "_static.html"
        $scope.template = { name: static_url, url:  '/rewards/Content/ad/' + static_url};//$scope.templates[0];
    }
    //console.log($scope.template)    ;

    init();

});