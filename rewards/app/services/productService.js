/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 24/6/13
 * Time: 9:28 PM
 * To change this template use File | Settings | File Templates.
 */
//This handles retrieving data and is used by controllers. 3 options (server, factory, provider) with
//each doing the same thing just structuring the functions/data differently.
app.service('productService', function () {
    this.get_productDetailsById = function ($http, $q, product_id) {
            var apiPath = cat_service_url +  '/store/prod_details?prod_id=' + product_id + '&json=true';

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: apiPath,
                //data: data,
                type: JSON
            }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject("An error occured while validating User");
                })

            return deferred.promise;
        };

    this.prod_view_insert = function ($http, $q, prod) {
        var apiPath = cat_service_url + '/audit/prod_view/insert/';
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: apiPath,
            data: prod,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);

            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;
    };


});