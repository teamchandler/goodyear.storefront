/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 17/1/14
 * Time: 10:05 AM
 * To change this template use File | Settings | File Templates.
 */
app.service('my_achievementService', function (localStorageService, utilService) {



    this.get_sales_achievement_data  = function ($http, $q,company, user_id) {
        var apiPath = '/tracker/sales_achievement_data?company=' + company + '&emp_id=' + user_id + '&json=true/'  ;
        return get_data($http, $q, user_id, apiPath)
    }


    var get_data = function ($http, $q, user_id, api) {
        var apiPath = cat_service_url + api ;
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: apiPath,
            type: JSON
        }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data)
            {
                deferred.reject("An error occured while validating User");
            })
        return deferred.promise;
    }


});