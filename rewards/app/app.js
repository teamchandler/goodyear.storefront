﻿/// <reference path="../Scripts/angular-1.1.4.js" />

/*#######################################################################
  
  Rahul Guha

  Normally like to break AngularJS apps into the following folder structure
  at a minimum:

  /rewards/app
      /controllers      
      /directives
      /services
      /partials
      /views

  #######################################################################*/

var app = angular.module('ECOMM', ['ngRoute', 'ui.bootstrap', 'LocalStorageModule']);
app.run(function ($rootScope, $location) {
    $rootScope.$on('$routeChangeSuccess', function () {
        ga('send', 'pageview', $location.path());
    });
});
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]);
//This configures the routes and associates each route with a view and a controller
app.config(function ($routeProvider) {
    $routeProvider

         
        .when('/customers',
            {
                controller: 'CustomersController',
                templateUrl: '/rewards/rewards/app/partials/customers.html'
            })
        .when('/cart',
        {
            controller: 'cartController',
            templateUrl: '/rewards/app/partials/cart_page.html'
        })
        .when('/checkout',
        {
            controller: 'checkoutController',
            templateUrl: '/rewards/app/partials/checkout.html'
        })
          .when('/order',
        {
            controller: 'orderController',
            templateUrl: '/rewards/app/partials/order.html'
        })

      //.when('/payment',
      //  {
      //      controller: 'paymentController',
      //      templateUrl: '/rewards/app/partials/Payment.html'
      //  })

        .when('/home',
        {
            controller: 'HomeController',
            templateUrl: '/rewards/app/partials/home_dummy.html'
        })
//        .when('/checkout_switch',
//        {
//            controller: 'Checkout_switchController' ,
//            templateUrl: '/rewards/app/partials/home_dummy.html'
//        })
        .when('/landing',
        {
            controller: 'landingController',
            templateUrl: '/rewards/app/partials/landing.html'
        })
        .when('/registration',
        {
            controller: 'registrationController',
            templateUrl: '/rewards/app/partials/Registration.html'
        })
        .when('/login',
        {
            controller: 'loginController',
            templateUrl: '/rewards/app/partials/login.html'
        })

         .when('/forgot_password',
        {
            controller: 'forgotpwdController',
            templateUrl: '/rewards/app/partials/forgot_password.html'
        })

       .when('/message',
        {
            controller: 'messageController',
            templateUrl: '/rewards/app/partials/message.html'
        })


         .when('/activation/:id/:cid',
        {
            controller: 'activationController',
            templateUrl: '/rewards/app/partials/accountActivation.html'
        })
        .when('/reset_password/:id/:cid',
        {
            controller: 'reset_passwordController',
            templateUrl: '/rewards/app/partials/reset_password.html'
        })
        //Define a route that has a route parameter in it (:customerID)
        .when('/customerorders/:customerID',
            {
                controller: 'CustomerOrdersController',
                templateUrl: '/rewards/app/partials/customerOrders.html'
            })
        //Define a route that has a route parameter in it (:customerID)
        .when('/orders',
            {
                controller: 'OrdersController',
                templateUrl: '/rewards/app/partials/orders.html'
            })
        //.when('/acct_points',
        //{
        //    controller: 'acct_pointsController',
        //    templateUrl: '/rewards/app/partials/acct_points.html'
        //})

        //.when('/myorder',
        //{
        //    controller: 'MyorderController',
        //    templateUrl: '/rewards/app/partials/myorder.html'
        //})

        .when('/acct_points',
        {
            controller: 'acct_pointsController',
            templateUrl: '/rewards/app/partials/acct_points.html'
        })

        .when('/myorder',
        {
            controller: 'MyorderController',
            templateUrl: '/rewards/app/partials/myorder.html'
        })

        .when('/my_profile',
        {
            controller: 'myprofileController',
            templateUrl: '/rewards/app/partials/my_profile.html'
        })
        .when('/achievement',
        {
            controller: 'my_achievementController',
            templateUrl: '/rewards/app/partials/my_achievement.html'
        })



        .when('/cat/:catId',
        {
            controller: 'CatController',
            templateUrl: '/rewards/app/partials/cat.html'
        })
        .when('/special/:catId',
        {
            controller: 'specialController',
            templateUrl: '/rewards/app/partials/special.html'
        })

        .when('/newarrivals',
        {
            controller: 'newarrivalsController',
            templateUrl: '/rewards/app/partials/newarrivals.html'
        })

        .when('/premium',
        {
            controller: 'premiumController',
            templateUrl: '/rewards/app/partials/premium.html'
        })

        .when('/express',
        {
            controller: 'expressController',
            templateUrl: '/rewards/app/partials/express.html'
        })

        .when('/cat1/:catId',
        {
            controller: 'Cat1Controller',
            templateUrl: '/rewards/app/partials/cat1.html'
        })
        .when('/brand/:brand_name',
        {
            controller: 'brandController',
            templateUrl: '/rewards/app/partials/brand.html'
        })
        .when('/cat/:cat_id/brand/:brand_name',
        {
            controller: 'CatController',
            templateUrl: '/rewards/app/partials/cat.html'
        })
        .when('/prod/:prodId',
        {
            controller: 'ProductDetailsController',
            templateUrl: '/rewards/app/partials/product.html'
        })
        .when('/search/:srch/:context',
        {
            controller: 'searchController',
            templateUrl: '/rewards/app/partials/search.html'
        })

        .when('/quickbuy/:prodId',
        {
            controller: 'quickbuyController',
            templateUrl: '/rewards/app/partials/quickbuy.html'
        })
        .when('/quickbuy/:prodId/:cart',
        {
            controller: 'quickbuyController',
            templateUrl: '/rewards/app/partials/quickbuy.html'
        })

        .when('/contact_us',
        {
            controller: 'contactUsController',
            templateUrl: '/rewards/app/partials/contact_us.html'
        })
        .when('/feedback',
        {
            controller: 'feedbackController',
            templateUrl: '/rewards/app/partials/feedback.html'
        })
        .when('/terms',
        {
            //            controller: 'ProductDetailsController',
            templateUrl: '/rewards/app/partials/terms.html'
        })
        .when('/aboutus',
        {
            //controller: 'registrationController',
            templateUrl: '/rewards/app/partials/aboutus.html'
        })
        .when('/return_policy',
        {
            //controller: 'registrationController',
            templateUrl: '/rewards/app/partials/return_policy.html'
        })

        .when('/security_policy',
        {
            //controller: 'registrationController',
            templateUrl: '/rewards/app/partials/security_policy.html'
        })

        .when('/privacy_policy',
        {
            //controller: 'registrationController',
            templateUrl: '/rewards/app/partials/privacy_policy.html'
        })


        .otherwise({ redirectTo: '/home' });
});




