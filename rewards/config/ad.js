/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 20/7/13
 * Time: 9:29 AM
 * To change this template use File | Settings | File Templates.
 */
var adItems = {
    "landing_page_main_scroller" : [{
        "href" : "#/cat/7.1.0",
        "link" : "https://s3.amazonaws.com/seep-banners/bigs.jpg"
    }],
    "landing_page_static_html" : "https://s3-ap-southeast-1.amazonaws.com/annectos/ads/men_static_ad.html",
    "offer_items" : [{
        "id" : "001",
        "offer_text" : "Only till 24th July midnight | Additional 10% off on Nike Shoes only for Intel employees",
        "href" : "#/cat/1.0/"
    }, {
        "id" : "002",
        "offer_text" : "Additional 5% off on Women Hand Bags only for Intel employees",
        "href" : "#/cat/2.0/"
    }],
    "l1_banner" : [{
        ]
};
