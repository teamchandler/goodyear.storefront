'use strict';

describe('Controller: SeepCtrl', function () {

  // load the controller's module
  beforeEach(module('chandlerschneiderApp'));

  var SeepCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SeepCtrl = $controller('SeepCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
