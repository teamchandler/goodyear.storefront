'use strict';

describe('Controller: MainBaseCtrl', function () {

  // load the controller's module
  beforeEach(module('chandlerschneiderApp'));

  var MainBaseCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainBaseCtrl = $controller('MainBaseCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
