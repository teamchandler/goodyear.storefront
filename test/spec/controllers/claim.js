'use strict';

describe('Controller: ClaimCtrl', function () {

  // load the controller's module
  beforeEach(module('chandlerschneiderApp'));

  var ClaimCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ClaimCtrl = $controller('ClaimCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
