'use strict';

describe('Controller: LoginBaseCtrl', function () {

  // load the controller's module
  beforeEach(module('chandlerschneiderApp'));

  var LoginBaseCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LoginBaseCtrl = $controller('LoginBaseCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
