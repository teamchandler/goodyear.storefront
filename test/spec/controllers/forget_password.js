'use strict';

describe('Controller: ForgetPasswordCtrl', function () {

  // load the controller's module
  beforeEach(module('chandlerschneiderApp'));

  var ForgetPasswordCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ForgetPasswordCtrl = $controller('ForgetPasswordCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
